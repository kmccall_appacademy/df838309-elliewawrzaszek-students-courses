class Student
  attr_accessor :first_name, :last_name, :courses
  def initialize(first_name, last_name, courses = [])
    @first_name = first_name
    @last_name = last_name
    @courses = Array.new
  end

  def name
    @first_name + " " + @last_name
  end

  def course_load
    dept_creds = {}
    @courses.each do |course|
      if dept_creds.keys.include?(course.department)
        dept_creds[course.department] += course.credits
      else
        dept_creds[course.department] = course.credits
      end
    end
      dept_creds
  end

  def enroll(new_course)
    raise "enrollment ERROR" if courses.any? {|course| course.conflicts_with?(new_course)}
    if !courses.include?(new_course)
      @courses << new_course
      new_course.students << self
    end
  end


end
